from setuptools import setup, find_packages

setup(
    name="complexVisualization",
    version="0.1",
    packages=find_packages(),
    namespace_package=["visualization", "visualization.complex"],
    install_requires=["SocialMediaApp>=0.1"],
    entry_points={
        "plugin.visualizations":
            ["complexVisualization=visualization.complex.complex_visualizator:ComplexVisualization"]
    },
    zip_safe=True
)
