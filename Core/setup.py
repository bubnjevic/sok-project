from setuptools import setup, find_packages


setup(
    name="SocialMediaApp",
    version="0.1",
    packages=find_packages(),
    install_requires=['Django>=4.0'],
    package_data={'SocialMediaApp': ['static/*.css', 'static/*.js', 'static/*.html', 'templates/*.html']},
    zip_safe=False
)