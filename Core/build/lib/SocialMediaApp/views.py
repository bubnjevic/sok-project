from django.shortcuts import render
from django.apps import apps
from django.http import HttpResponse
from django.shortcuts import render, redirect
from _operator import gt, lt, ge, le, eq, ne
import datetime
import time
# Create your views here.


def setData(request, id):
    initial_load_plugin = "load_json_file"
    selected_plugin_id = id
    all_plugins = apps.get_app_config('SocialMediaApp').all_plugins
    if selected_plugin_id == "":
        for plugin in all_plugins:
            if plugin.identifier() == initial_load_plugin:
                path = 'D:\Programiranje\Treca godina\Prvi semestar\SOFTVERSKI OBRASCI I KOMPONENTE\ProjekatV3\sok-project\Data\\facebook.json'
                apps.get_app_config('SocialMediaApp').graph = plugin.load(path)
    else:
        for plugin in all_plugins:
            if plugin.identifier() == selected_plugin_id:
                if selected_plugin_id == initial_load_plugin:
                    path = 'D:\Programiranje\Treca godina\Prvi semestar\SOFTVERSKI OBRASCI I KOMPONENTE\ProjekatV3\sok-project\Data\\facebook.json'

                else:
                    path = 'D:\Programiranje\Treca godina\Prvi semestar\SOFTVERSKI OBRASCI I KOMPONENTE\ProjekatV3\sok-project\Data\\data.yaml'
                apps.get_app_config('SocialMediaApp').graph = plugin.load(path)
    # currentVisualization != None odvedi na tu funkciju za prikaz
    return redirect('index')


def complex_visualization(request):
    visualization_plugins = apps.get_app_config('SocialMediaApp').visualization_plugin
    data_plugins = apps.get_app_config('SocialMediaApp').source_plugin
    nodes, root = treeViewData()

    for plugin in visualization_plugins:
        if plugin.identifier() == "ComplexVisualization":
            apps.get_app_config('SocialMediaApp').current_visualization = plugin
            return HttpResponse(
                plugin.visualize(apps.get_app_config('SocialMediaApp').graph, data_plugins, nodes, root, request))

    return redirect('index')

def simplex_visualization(request):
    visualization_plugins = apps.get_app_config('SocialMediaApp').visualization_plugin
    data_plugins = apps.get_app_config('SocialMediaApp').source_plugin
    nodes, root = treeViewData()

    for plugin in visualization_plugins:
        if plugin.identifier() == "SimplexVisualization":
            apps.get_app_config('SocialMediaApp').current_visualization = plugin
            return HttpResponse(
                plugin.visualize(apps.get_app_config('SocialMediaApp').graph, data_plugins, nodes, root, request))

    return redirect('index')

def search_graph(request):
    query_value = request.POST.get("search_input", "")
    if query_value == "":
        return backToCurrentVisualization(request)
    graph = apps.get_app_config('SocialMediaApp').graph
    if not is_graph_loaded(graph):
        return backToCurrentVisualization(request)
    for v in graph.vertices:
        if graph.vertices[v].has_been_found:
            attributes = graph.vertices[v].attributes.values()
            contains_query = False
            for attr in attributes:
                if is_object(attr):
                    for value in attr:
                        contains_query = str(query_value).lower() in str(value).lower()
                        if contains_query:
                            print(value)
                            break
                else:
                    contains_query = str(query_value).lower() in str(attr).lower()
                if contains_query:
                    print(attr)
                    break
            graph.vertices[v].has_been_found = contains_query
    apps.get_app_config('SocialMediaApp').graph = graph

    return backToCurrentVisualization(request)


def backToCurrentVisualization(request):
    data_plugins = apps.get_app_config('SocialMediaApp').source_plugin
    nodes, roots = treeViewData()
    current_visualize = apps.get_app_config('SocialMediaApp').current_visualization
    return HttpResponse(current_visualize.visualize(apps.get_app_config('SocialMediaApp').graph, data_plugins, nodes, roots, request))

def filter_graph(request):
    request_value = request.POST.get("filter_input", "")
    graph = apps.get_app_config('SocialMediaApp').graph

    if not is_graph_loaded(graph):
        return backToCurrentVisualization(request)
    if not is_request_value_valid(request_value, graph):
        return backToCurrentVisualization(request)

    attribute_name, attribute_value, operator = get_request_parameters(request_value)
    for v in graph.vertices:
        if graph.vertices[v].has_been_found:
            if attribute_name in graph.vertices[v].attributes:
                value_from_graph = graph.vertices[v].attributes[attribute_name]
                compare_values(graph, v, value_from_graph, attribute_value, operator)
            # if the entered attribute name doesn't exist, node won't be shown
            else:
                graph.vertices[v].has_been_found = False

    return backToCurrentVisualization(request)

def compare_values(graph, v, value_from_graph, attribute_value, operator):
    operators = {'==': eq, '<': lt, '<=': le, '>': gt, '>=': ge, '!=': ne}
    o = operators[operator]
    if attribute_value.isnumeric() and value_from_graph.isnumeric():
        if not o(float(value_from_graph), float(attribute_value)):
            graph.vertices[v].has_been_found = False
    elif is_date(attribute_value) and is_date(value_from_graph):
        date_value_from_graph = time.strptime(value_from_graph, "%d-%m-%Y")
        date_attribute_value = time.strptime(attribute_value, "%d-%m-%Y")
        if not o(date_value_from_graph, date_attribute_value):
            graph.vertices[v].has_been_found = False
    elif is_time(attribute_value) and is_time(value_from_graph):
        time_value_from_graph = time.strptime(value_from_graph, "%H:%M:%S")
        time_attribute_value = time.strptime(attribute_value, "%H:%M:%S")
        if not o(time_value_from_graph, time_attribute_value):
            graph.vertices[v].has_been_found = False
    elif isinstance(value_from_graph, list):
        if operator != "!=":
            exists_in_list = False
            for value in value_from_graph:
                if attribute_value.isnumeric() and value.isnumeric():
                    if o(float(value), float(attribute_value)):
                        exists_in_list = True
                        break
                elif is_date(attribute_value) and is_date(value):
                    date_value_from_graph = time.strptime(value, "%d-%m-%Y")
                    date_attribute_value = time.strptime(attribute_value, "%d-%m-%Y")
                    if o(date_value_from_graph, date_attribute_value):
                        exists_in_list = True
                        break
                elif is_time(attribute_value) and is_time(value):
                    time_value_from_graph = time.strptime(value, "%H:%M:%S")
                    time_attribute_value = time.strptime(attribute_value, "%H:%M:%S")
                    if o(time_value_from_graph, time_attribute_value):
                        exists_in_list = True
                        break
                else:
                    if o(value, attribute_value):
                        exists_in_list = True
                        break
        else:
            exists_in_list = True
            op = operators["=="]
            for value in value_from_graph:
                if attribute_value.isnumeric() and value.isnumeric():
                    if op(float(value), float(attribute_value)):
                        exists_in_list = False
                        break
                elif is_date(attribute_value) and is_date(value):
                    date_value_from_graph = time.strptime(value, "%d-%m-%Y")
                    date_attribute_value = time.strptime(attribute_value, "%d-%m-%Y")
                    if op(date_value_from_graph, date_attribute_value):
                        exists_in_list = False
                        break
                elif is_time(attribute_value) and is_time(value):
                    time_value_from_graph = time.strptime(value, "%H:%M:%S")
                    time_attribute_value = time.strptime(attribute_value, "%H:%M:%S")
                    if op(time_value_from_graph, time_attribute_value):
                        exists_in_list = False
                        break
                else:
                    if op(value, attribute_value):
                        exists_in_list = False
                        break
        if not exists_in_list:
            graph.vertices[v].has_been_found = False
    else:
        if not o(value_from_graph, attribute_value):
            graph.vertices[v].has_been_found = False

def is_date(potential_date):
    is_valid = True
    format = "%d-%m-%Y"
    try:
        is_valid = bool(datetime.datetime.strptime(potential_date, format))
        return is_valid
    except ValueError:
        is_valid = False
        return is_valid

def is_time(potential_time):
    is_valid = True
    format = "%H:%M:%S"
    try:
        is_valid = bool(datetime.datetime.strptime(potential_time, format))
        return is_valid
    except:
        is_valid = False
        return is_valid

def is_request_value_valid(request_value, graph):
    request_value.strip()
    request_parameters = request_value.split(" ")

    operators = ['<', '<=', '>', '>=', '==', '!=']
    attribute_names = []

    for v in graph.vertices:
        for key in graph.vertices[v].attributes:
            if key not in attribute_names:
                attribute_names.append(key)

    if len(request_parameters) != 3:
        return False
    elif request_parameters[1] not in operators:
        return False
    elif request_parameters[0] not in attribute_names:
        return False
    return True

def get_request_parameters(request_value):
    request_parameters = request_value.split(" ")
    attribute_name = request_parameters[0]
    operator = request_parameters[1]
    attribute_value = request_parameters[2]
    return attribute_name, attribute_value, operator

def createChildrenList(node, graph):
    children = []
    for child in node.children:
        try:
            child = findChild(child.id, graph)
            if child is not None and child.has_been_found:
                children.append({"id": "np_" + child.id})
        except:
            found_child = findChild(child, graph)
            if child is not None:
                children.append({"id": "np_" + str(found_child.id)})
            
    return children

def findChild(childId, graph):
    for key in graph.vertices.keys():
        if key == childId:
            return graph.vertices[key]

    return None

def treeViewData():
    graph = apps.get_app_config('SocialMediaApp').graph
    nodes = []
    roots = []

    for key in graph.vertices.keys():
        if graph.vertices[key].has_been_found:
            children = createChildrenList(graph.vertices[key], graph)
            nodes.append({"id": "np_" + str(key),
                          "children": children,
                          "is_active": False,
                          "has_been_found": graph.vertices[key].has_been_found,
                          "attributes": graph.vertices[key].attributes})

    for key in graph.vertices.keys():
        if graph.vertices[key].has_been_found:
            is_target = False
            for edge in graph.edges:
                if key == edge.id2:
                    is_target = True
                    break
            if not is_target:
                roots.append({"id": "np_" + str(graph.vertices[key].id),
                              "children": createChildrenList(graph.vertices[key], graph),
                              "has_been_found": graph.vertices[key].has_been_found,
                              "attributes": graph.vertices[key].attributes})

    if len(roots) == 0:
        for node in nodes:
            roots.append(node)
    return nodes, roots

def index(request):
    source_plugins = apps.get_app_config('SocialMediaApp').source_plugin
    return render(request, 'index.html', {"dataPlugins": source_plugins})

def is_graph_loaded(graph):
    if len(graph.vertices) == 0:
        return False
    return True

def is_object(attr):
    return isinstance(attr, list)
