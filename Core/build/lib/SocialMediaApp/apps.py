from django.apps import AppConfig
from SocialMediaApp.models import Graph
import pkg_resources


class SocialMediaAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'SocialMediaApp'
    verbose_name = 'SocialMediaApp'
    graph = Graph()
    all_plugins = []
    current_visualization = None
    source_plugin = None
    visualization_plugin = None


    def ready(self):
        self.source_plugin = load_plugins("plugin.loading")
        self.visualization_plugin = load_plugins("plugin.visualizations")
        self.all_plugins = self.source_plugin + self.visualization_plugin
        print("=" * 50)
        print("MY SOURCE PLUGINS: ", self.source_plugin)
        print("=" * 50)
        print("MY VIEW PLUGINS: ", self.visualization_plugin)


def load_plugins(plugin_type):
    plugins = []

    for entry_point in pkg_resources.iter_entry_points(group=plugin_type):
        p = entry_point.load()
        plugin = p()
        plugins.append(plugin)
    return plugins

