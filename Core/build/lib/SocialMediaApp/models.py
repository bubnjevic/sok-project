from django.db import models

# Create your models here.
class Graph:
    def __init__(self):
        self._vertices = {}
        self._edges = []

    @property
    def vertices(self):
        return self._vertices

    @vertices.setter
    def vertices(self, vertices):
        self._vertices = vertices

    @property
    def edges(self):
        return self._edges

    @edges.setter
    def edges(self, edges):
        self._edges = edges

    def add_vertex(self, vertex):
        self._vertices[vertex.id] = vertex

    def add_edge(self, edge):
        self._edges.append(edge)


class Vertex:
    def __init__(self):
        self._id = "-1"
        self._attributes = {}
        self._has_been_found = True
        self._children = []

    @property
    def children(self):
        return self._children

    @children.setter
    def children(self, children):
        self._children = children

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, identification):
        self._id = identification

    @property
    def attributes(self):
        return self._attributes

    @attributes.setter
    def attributes(self, attributes):
        self._attributes = attributes

    @property
    def has_been_found(self):
        return self._has_been_found

    @has_been_found.setter
    def has_been_found(self, has_been_found):
        self._has_been_found = has_been_found

    def __str__(self):
        attributes_values = list(self._attributes.values())
        attributes_str = attributes_values[0] if attributes_values else "no attributes"
        return f"id: {self._id}, attributes: {attributes_str}"


class Edge:
    def __init__(self):
        self._id1 = "-1"
        self._id2 = "-1"

    @property
    def id1(self):
        return self._id1

    @id1.setter
    def id1(self, id1):
        self._id1 = id1

    @property
    def id2(self):
        return self._id2

    @id2.setter
    def id2(self, id2):
        self._id2 = id2

    def __str__(self):
        return f"id1: {self._id1}, id2: {self._id2}"
