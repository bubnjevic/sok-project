import abc


class LoadService(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def name(self):
        pass

    @abc.abstractmethod
    def identifier(self):
        pass

    @abc.abstractmethod
    def load(self, path):
        pass
