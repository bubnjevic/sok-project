from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('complexVisualization', views.complex_visualization, name='complex_visualization'),
    path('simplexVisualization', views.simplex_visualization, name='simplex_visualization'),
    path('loadData/<str:id>', views.setData, name='load_selected_data'),
    path('search', views.search_graph, name='search_data'),
    path('filter', views.filter_graph, name='filter_data'),

]
