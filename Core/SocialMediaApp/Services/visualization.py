from abc import ABC, abstractmethod


class VisualizationService(ABC):
    @abstractmethod
    def name(self):
        pass

    @abstractmethod
    def identifier(self):
        pass

    @abstractmethod
    def visualize(self, data, data_plugins, nodes, root, request):
        pass

