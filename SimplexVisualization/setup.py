from setuptools import setup, find_packages

setup(
    name="simplexVisualization",
    version="0.1",
    packages=find_packages(),
    install_requires=["SocialMediaApp>=0.1"],
    entry_points={
        "plugin.visualizations":
            ["simplexVisualization=visualization.simplex.simplex_visualizator:SimplexVisualization"]
    },
    zip_safe=True
)
