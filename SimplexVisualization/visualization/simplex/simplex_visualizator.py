import json
from SocialMediaApp.Services.visualization import VisualizationService
from django.template import engines
from django.template.loader import render_to_string


class SimplexVisualization(VisualizationService):
    def name(self):
        return "Simplex Visualization"

    def identifier(self):
        return "SimplexVisualization"

    def visualize(self, data, data_plugins, nodes, root, request):
        graph = data
        vertices = {}
        edges = []


        for key in graph.vertices.keys():
            if not graph.vertices[key].has_been_found:
                continue
            attributes = []
            for attr_key in graph.vertices[key].attributes.keys():
                attributes.append(attr_key + ":" + str(graph.vertices[key].attributes[attr_key]))
            vertices[key] = {
                "id": "np_" + str(key),
                "attributes": attributes,
                "node_id": str(graph.vertices[key].id),
                "has_been_found": graph.vertices[key].has_been_found
            }
        for i in range(len(graph.edges)):
            for key in vertices.keys():
                if str(graph.edges[i].id1) == str(vertices[key].get("node_id")):
                    for k in vertices.keys():
                        if str(graph.edges[i].id2) == str(vertices[k].get("node_id")):
                            edges.append({
                                "source": str(vertices[key].get("node_id")),
                                "target": str(vertices[k].get("node_id"))
                            })


        template = """
                {% extends "treeView.html" %}
                {% block mainView %}
                <svg width="100%" height="100%" id = "mainView"></svg>
                <script type="text/javascript" src="https://d3js.org/d3.v3.js"></script>
                <script type="text/javascript">

                var nodesGraph =JSON.parse("{{vertices |escapejs}}");                
                var linksGraph= JSON.parse("{{edges |escapejs}}");

                let zoomedNodes;


                linksGraph.forEach(function(linkGraph) {
                    linkGraph.source = nodesGraph[linkGraph.source];
                    linkGraph.target = nodesGraph[linkGraph.target];
                });

                var force = d3.layout.force() //kreiranje force layout-a
                            .size([1000, 1000]) //raspoloziv prostor za iscrtavanje
                            .nodes(d3.values(nodesGraph)) //dodaj cvorove
                            .links(linksGraph) // dodaj grane
                            .on("tick", tick) //sta treba da se desi kada su izracunate nove pozicija elemenata
                            .linkStrength(1) // snaga razmaka izmedju elemenata
                            .linkDistance(1000) //razmak izmedju elemenata
                            .charge(-3000)//koliko da se elementi odbijaju
                            .start(); //pokreni izracunavanje pozicija

                // dodavanje pan i zoom
                var svgMain = d3.select("#mainView")
                        .call(d3.behavior.zoom().on("zoom", function () {
                                svgMain.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")")
                        })).append('g');

                var drag = force.drag().on("dragstart", dragstart);


                // dodavanje linkova
                var link = svgMain.selectAll('.link')
                        .data(linksGraph)
                        .enter().append('line')
                        .attr('class', 'link')
                        .style("stroke", "black");

                var node = svgMain.selectAll('.node')
                        .data(force.nodes()) //add
                        .enter().append('g')
                        .attr('class', 'node')
                        .attr('id', function(d) { return d.id; })
                        .on('click', function(d) {
                            onNodeClick(d);
                        });

                d3.selectAll('.node').each(function(d){ simplexVisualization(d); });

                function simplexVisualization(d){
                    var textSize = 12;
                    var height = 30;
                    var width = 50;

                    // Inserting the rectangle
                    d3.select("g#"+d.id).append('rect')
                        .attr('x', 0).attr('y', 0).attr('width', width).attr('height', height)
                        .attr('fill', d => (d.has_been_found ? '#334d4d' : '#d1e0e0'));

                    // Inserting the node name or username
                    d3.select("g#"+d.id).append('text').attr('x', width / 2).attr('y', textSize)
                        .attr('text-anchor','middle')
                        .attr('font-size',textSize).attr('font-family','sans-serif')
                        .attr('fill','white').text(d.node_id);
                }


                function tick(e) {
                        node.attr("transform", function(d) {return "translate(" + d.x + "," + d.y + ") ";})
                            .call(force.drag);

                        link.attr('x1', function(d) { return d.source.x; })
                            .attr('y1', function(d) { return d.source.y; })
                            .attr('x2', function(d) { return d.target.x; })
                            .attr('y2', function(d) { return d.target.y; });

                }

                function dragstart(d) {
                        d3.event.sourceEvent.stopPropagation();
                        d3.select(this).classed("fixed", d.fixed = true);
                }

                function onNodeClick(d){
                        let attributes = "ID:" + d.id + '\\n';
                        for (let attribute of d.attributes) {
                            attributes += attribute + '\\n';
                        }
                        alert(attributes);
                        if (zoomedNodes) {
                            zoomedNodes.nodes.selectAll('circle').attr('fill', zoomedNodes.old_color); //vracanje prethodno obelezenih cvorova na staro
                            zoomedNodes.nodes.selectAll('text').attr('fill', zoomedNodes.old_color);
                        }
                        try {
                            let mainNodes = d3.selectAll('.' + d.id);
                            let old_color = mainNodes.selectAll('circle').attr('fill');
                            zoomedNodes = {"nodes" : mainNodes, "old_color": old_color};
                            mainNodes.selectAll('circle').attr('fill', '#990000'); //postavljanje nove boje odgovarajucih cvorova
                            mainNodes.selectAll('text').attr('fill', '#990000');
                        } catch (error) { }
                }


                </script>
                {% endblock %}
        """
    
        django_engine = engines['django']
        template_html = django_engine.from_string(template)
        template_html = template_html.render({
            "dataPlugins": data_plugins,
            "vertices": json.dumps(vertices),
            "edges": json.dumps(edges),
            "treeNodes": json.dumps(nodes),
            "treeRoot": json.dumps(root)}, request)
        return template_html
