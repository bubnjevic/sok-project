#!/bin/bash

# This script is used to clean unnecessary generated files/folders.

remove_eggs() {
  # The directory path is sent as the first argument
  # shellcheck disable=SC2164
  cd "$1"
  rm -rf build
  # shellcheck disable=SC2035
  rm -rf *.egg-info
  rm -rf dist
  # shellcheck disable=SC2103
  cd ..
}

# remove build files from components
remove_eggs Core
remove_eggs JsonParser
remove_eggs YamlParser
remove_eggs ComplexVisualization

# remove db
# shellcheck disable=SC2164
cd SocialNetworks
# shellcheck disable=SC2035
rm *.sqlite3
# shellcheck disable=SC2103
cd ..
