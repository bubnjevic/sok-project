@echo off

rem This script is used to clean unnecessary generated files/folders.
cd .\Core
echo Removing Core
for /D %%d in (*.egg-info) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (build) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (dist) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
cd ..

cd .\JsonParser
echo Removing JsonParser
for /D %%d in (*.egg-info) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (build) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (dist) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
cd ..

cd .\YamlParser
echo Removing YamlParser
for /D %%d in (*.egg-info) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (build) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (dist) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
cd ..

cd .\ComplexVisualization
echo Removing ComplexVisualization
for /D %%d in (*.egg-info) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (build) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (dist) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
cd ..

cd .\SimplexVisualization
echo Removing SimplexVisualization
for /D %%d in (*.egg-info) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (build) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
for /D %%d in (dist) do (
    if exist "%%d" (
        echo Removing directory: %%d
        rd /s /q "%%d"
    )
)
cd ..

cd .\SocialNetworks
echo Removing SocialNetworks
if exist *.sqlite3 del *.sqlite3
cd ..