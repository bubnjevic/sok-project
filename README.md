# Projektni zadatak iz predmeta Softverski obrasci i komponente

## Članovi tima
* Veljko Bubnjević       SV 51/2020
* Aleksandra Balažević   SV 37/2020
<hr>

## Uputstvo za pokretanje

Za pokretanje aplikacije biće neophodno instalirati biblioteke u requirements.txt fajlu.
<br>
Najbolji način bi bilo kreiranje virtuelnog okruženja koristeći komandu ```
virtuelenv``` i u njemu pokrenuti komandu ```pip install requirements.txt```. 
Projekat se može pokrenuti pokretanje shell skripte [run.sh](https://gitlab.com/bubnjevic/sok-project/-/blob/main/run.sh?ref_type=heads)
(Linux, MacOS) ili bat skripte [run.but](https://gitlab.com/bubnjevic/sok-project/-/blob/main/run.bat?ref_type=heads) (Windows).
.