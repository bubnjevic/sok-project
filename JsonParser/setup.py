from setuptools import setup, find_packages

setup(
    name="json_parser",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['LoadingData', 'LoadingData.LoadFromJSON'],
    install_requires=['SocialMediaApp>=0.1'],
    entry_points={
        'plugin.loading':
            ['json_parser=LoadingData.LoadFromJSON.load:LoadJSONData'],
    },
    zip_safe=True
)
