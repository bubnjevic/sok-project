
from SocialMediaApp.Services.load import LoadService
from SocialMediaApp.models import *
import json


def read_json_file(file_path):
    with open(file_path, 'r') as file:
        try:
            data = json.load(file)
            return data
        except json.JSONDecodeError as e:
            print(f"Error reading JSON file {file_path}: {e}")
            return None


def fill_graph(data, graph, parent_vertex=None, parent_id=None):
    def process_element(element, parent_vertex, parent_id):
        # Ako element ima 'id', koristi ga za ID čvora, inače generiši novi ID
        vertex_id = str(element.get('id', hash(str(element))))
        
        # Proveri da li čvor sa ovim ID-om već postoji u grafu
        existing_vertex = graph.vertices.get(vertex_id)
        if existing_vertex:
            vertex = existing_vertex
        else:
            vertex = Vertex()
            vertex.id = vertex_id
            graph.add_vertex(vertex)
        
        # Ako postoji roditeljski čvor, dodaj vezu između roditelja i trenutnog čvora
        if parent_vertex:
            edge = Edge()
            edge.id1 = parent_id
            edge.id2 = vertex_id
            
            if not any(e.id1 == edge.id1 and e.id2 == edge.id2 for e in graph.edges):
                graph.add_edge(edge)
            
            # Dodaj trenutni čvor kao dijete roditeljskom čvoru
            if not vertex in parent_vertex.children:
                parent_vertex.children.append(vertex)
            if not parent_vertex in vertex.children:
                vertex.children.append(parent_vertex)

        
        # Postavi atribute trenutnog čvora
        for key, value in element.items():
            if isinstance(value, list):
                # Rekurzivno pozovi sebe za svaku vrednost koja je lista
                for child in value:
                    process_element(child, vertex, vertex_id)
            elif isinstance(value, dict):
                # Rekurzivno pozovi sebe za svaku vrednost koja je rečnik
                process_element(value, vertex, vertex_id)
            elif key != 'id':
                # Dodaj atribute trenutnog čvora
                vertex.attributes[key] = value
    
    # Proveri da li je glavni element lista ili objekat
    if isinstance(data, list):
        for element in data:
            process_element(element, parent_vertex, parent_id)
    elif isinstance(data, dict):
        process_element(data, parent_vertex, parent_id)


class LoadJSONData(LoadService):

    def name(self):
        return "JSON data parser"

    def identifier(self):
        return "load_json_file"

    def load(self, path):
        parsed_data = read_json_file(path)
        graph = Graph()
        fill_graph(parsed_data, graph)

        print("Vertices:")
        for vertex_id, vertex in graph.vertices.items():
            print(f"{vertex_id}: {vertex}")

        print("*" * 50)


        print("\nEdges:")

        for edge in graph.edges:
            print(f"{edge}")
        return graph


if __name__ == "__main__":
    loader = LoadJSONData()
    x = loader.load('../../Data/facebook.json')
