import json
import random
from faker import Faker

import json
import random
from faker import Faker


def create_json(n):

    fake = Faker()

    profiles = []
    for i in range(1, n):
        friends = random.sample(profiles, min(random.randint(0, min(3, i)), len(profiles)))
        friends = [{"id": friend["id"],
                    "name": friend["name"],
                    "friends" : friend["friends"],
                    } for friend in friends]

        first_name = fake.first_name().lower()
        last_name = fake.last_name().lower()
        facebook_username = f"{first_name}.{last_name}"

        profile = {
            "id": i,
            "name": facebook_username,
            "friends": friends,
        }

        # Dodavanje dodatnih atributa po potrebi
        if random.random() > 0.5:
            profile["gender"] = random.choice(["male", "female"])
        if random.random() > 0.5:
            profile["profile_picture"] = fake.image_url()
        # if random.random() > 0.5:
        #     profile["bio"] = fake.paragraph()
        if random.random() > 0.5:
            profile["date_of_birth"] = fake.date_between(start_date="-50y", end_date="-18y").strftime("%Y-%m-%d")
        if random.random() > 0.5:
            profile["location"] = fake.city() + ', ' + fake.country()
        if random.random() > 0.5:
            profile["website_url"] = fake.url()
        if random.random() > 0.5:
            profile["email"] = fake.email()

        profiles.append(profile)

    data = {"profiles": profiles}
    return data


# Ostatak koda ostaje nepromenjen

def write_json(data, path):
    with open(path, "w") as f:
        json.dump(data, f, indent=2)


def read_json(path):
    with open(path, 'r') as f:
        try:
            parsed_json = json.load(f)
            print(json.dumps(parsed_json, indent=2))
        except json.JSONDecodeError as exc:
            print(exc)


if __name__ == "__main__":
    data = create_json(200)
    write_json(data, "../../../Data/facebook.json")
