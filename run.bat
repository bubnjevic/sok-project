@echo off

cd Core
echo Installing Core
python setup.py install
cd ..

cd ComplexVisualization
echo Installing ComplexVisualization
python setup.py install
cd ..

cd SimplexVisualization
echo Installing SimplexVisualization
python setup.py install
cd ..


cd YamlParser
echo Installing YamlParser
python setup.py install
cd ..

cd JsonParser
echo Installing JsonParser
python setup.py install
cd ..

cd SocialNetworks
echo Running Server
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
cd ..

