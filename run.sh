#!/bin/bash

# This script is used to build all necessary python components
# and runt django website.

lay_egs() {
  # The directory path is sent as the first argument
  # shellcheck disable=SC2164
  cd "$1"
  # shellcheck disable=SC2164
  pwd
  python3 setup.py install
  # shellcheck disable=SC2103
  cd ..
}

run_server() {
  # The Django website path is sent as the first argument
  # shellcheck disable=SC2164
  cd "$1"
  python3 manage.py makemigrations
  python3 manage.py migrate
  python3 manage.py runserver
}

# clean
source clean.sh

# build components
lay_egs Core
lay_egs JsonParser
lay_egs YamlParser
lay_egs ComplexVisualization
run_server SocialNetworks
