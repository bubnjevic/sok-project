from setuptools import setup, find_packages

setup(
    name="yaml_parser",
    version="0.1",
    packages=find_packages(),
    namespace_packages=['LoadingDataYAML', 'LoadingDataYAML.LoadFromYAML'],
    install_requires=['SocialMediaApp>=0.1'],
    entry_points = {
        'plugin.loading':
            ['yaml_parser=LoadingDataYAML.LoadFromYAML.load:LoadData'],
    },
    zip_safe=True
)