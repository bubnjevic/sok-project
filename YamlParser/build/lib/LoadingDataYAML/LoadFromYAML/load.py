import yaml
from SocialMediaApp.Services.load import LoadService
from SocialMediaApp.models import *


def read_yaml_file(file_path):
    with open(file_path, 'r') as file:
        try:
            data = yaml.safe_load(file)
            return data
        except yaml.YAMLError as e:
            print(f"Error reading YAML file {file_path}: {e}")
            return None


def sort_list(current_dictionary):
    sorted_list = []
    for key in current_dictionary:
        if (isinstance(current_dictionary[key], list) or isinstance(current_dictionary[key],
                                                                    dict)) and key != "referenceList":
            sorted_list.append(key)
        elif key != "referenceList":
            sorted_list.insert(0, key)
    if "referenceList" in current_dictionary:
        sorted_list.append("referenceList")
    return sorted_list


class LoadData(LoadService):
    def name(self):
        return "YAML data parser"

    def identifier(self):
        return "yaml_parser"

    def load(self, path):
        parsed_data = read_yaml_file(path)
        graph = Graph()
        self.fill_graph(parsed_data, graph)
        return graph

    def fill_graph(self, data, graph):
        data = data['elements']
        for i in range(0, len(data)):
            v = Vertex()
            for attribute in data[i]:
                if attribute == "following":
                    following = data[i].get('following', [])
                    for following_id in following:
                        e = Edge()
                        e.id1 = v.id
                        e.id2 = following_id
                        graph.add_edge(e)
                        v.children.append(e.id2)
                if attribute != "followers":
                    if attribute == "id":
                        v.id = data[i][attribute]
                    else:
                        v.attributes[attribute] = data[i][attribute]
            graph.add_vertex(v)
        for user1 in data:
            for user2 in data:
                user1_id = user1["id"]
                user2_following = user2["following"]
                if user1_id in user2_following:
                    user2_id = user2["id"]
                    e = Edge()
                    e.id1 = user2_id
                    e.id2 = user1_id
                    graph.add_edge(e)


if __name__ == "__main__":
    loader = LoadData()
    x = loader.load('D:\Programiranje\Treca godina\Prvi semestar\SOFTVERSKI OBRASCI I KOMPONENTE\ProjekatV3\sok-project\Data\\data.yaml')
    print(x.edges)
    print("*"*50)
    print(x.vertices)