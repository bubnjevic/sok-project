import random
import yaml
from faker import Faker

def createYAML(n):
    fake = Faker()
    elements = []
    for i in range(1, n):
        following = random.sample(range(1, n), random.randint(0, n - 1))
        if i in following:
            following.remove(i)
        element = {
            "id": i,
            "username": fake.user_name(),
            "following": following,
        }
        if random.random() > 0.5:
            element["gender"] = random.choice(["male","female"])
        if random.random() > 0.5:
            element["profile_picture"] = fake.image_url()
        if random.random() > 0.5:
            element["bio"] = fake.paragraph()
        if random.random() > 0.5:
            element["date_of_birth"] = fake.date_between(start_date="-50y", end_date="-18y").strftime("%Y-%m-%d")
        if random.random() > 0.5:
            element["location"] = fake.city() + ', ' + fake.country()
        if random.random() > 0.5:
            element["website_url"] = fake.url()
        if random.random() > 0.5:
            element["email"] = fake.email()
        
        elements.append(element)

    # Adding followers attribute to each element
    for i in range(len(elements)):
        followers = []
        for j in range(len(elements)):
            id = elements[i]['id']
            following = elements[j]["following"]
            if id in following:
                followers.append(elements[j]['id'])
        elements[i]['followers'] = followers

    data = {"elements": elements}
    return data


def writeYAML(data, path):
    yaml_data = yaml.dump(data,default_flow_style=False, sort_keys=False)

    with open(path, "w") as f:
        f.write(yaml_data)


def readYAML(path):
    with open(path, 'r') as stream:
        try:
            parsed_yaml=yaml.safe_load(stream)
            print(parsed_yaml)
        except yaml.YAMLError as exc:
            print(exc)

if __name__ == "__main__":
    data = createYAML(20)
    writeYAML(data, "data.yaml")